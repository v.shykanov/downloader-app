import React, { Component } from 'react';
import axios from 'axios';

export default class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jobs: false,
        };
    }

    componentDidMount() {
        if (this.state.jobs === false) {
            axios.get('/api/v1/jobs')
                .then((response) => {
                    this.state.jobs = response.data;
                });
        }
    }

    render() {
        return (
            <div>
            <h4>Jobs List</h4>

                {this.state.jobs &&
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Url</th>
                        <th scope="col">Download Link</th>
                        <th scope="col">Status</th>
                        <th scope="col">started_at</th>
                        <th scope="col">finished_at</th>
                    </tr>
                    </thead>
                    <tbody>

                    {this.state.jobs.map((v) =>
                    <tr>
                        <td>{v.id}</td>
                        <td>{v.url}</td>
                        <td>{v.link}</td>
                        <td>{v.human_status}</td>
                        <td>{v.started_at}</td>
                        <td>{v.finished_at}</td>
                    </tr>
                    )}

                    </tbody>
                </table>
                ||
                <p>Job queue is empty.</p>
                }
        </div>
        );

    }
}
