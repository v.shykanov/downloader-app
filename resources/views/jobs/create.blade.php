@extends('layouts.layout')

@section('title', 'Enqueue New Job')

@section('content')
    <h1 class="title">Enqueue New Job</h1>

    @if ($errors->any())
        <div class="notification is-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if ($message = Session::get('enqueued'))
        <div class="alert alert-info alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <form method="POST" action="/jobs">
        {{ csrf_field()  }}

        <div class="form-group">
            <label for="url">URL of the resource:</label>
            <input type="text" class="form-control" id="url" name="url" placeholder="URL" required>
        </div>
        <button type="submit" class="btn btn-primary">Enqueue</button>
    </form>

@endsection
