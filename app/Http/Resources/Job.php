<?php

namespace App\Http\Resources;

use App\Http\Resources\File as FileResource;

class Job extends AbstractResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'url' => $this->resource->url,
            'status' => $this->resource->status,
            'human_status' => $this->resource->human_status,
            'started_at' => $this->resource->started_at,
            'finished_at' => $this->resource->finished_at,
            'error' => $this->resource->error,
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
            'file' => FileResource::make($this->resource->file)
        ];
    }
}
