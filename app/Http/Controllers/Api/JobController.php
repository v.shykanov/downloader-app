<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Job\Show as ShowJobRequest;
use App\Http\Requests\Api\Job\Store as StoreJobRequest;
use App\Http\Resources\Job as JobResource;
use App\Models\Job as JobModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Resource as ResourceService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Psr\Container\ContainerInterface;

class JobController extends Controller
{
    /**
     * @var ResourceService
     */
    private $resourceService;

    /**
     * @param ContainerInterface $container
     * @return void
     */
    public function __construct(ContainerInterface $container)
    {
        $this->resourceService = $container->get('services.resource');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        return JobResource::collection(JobModel::all());
    }

    /**
     * @param StoreJobRequest $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StoreJobRequest $request): Response
    {
        $this->resourceService->createDownloadJob($request->get('url'));

        return response([
            'status' => true,
            'message' => 'Job to download resource has been enqueued.',
        ], JsonResponse::HTTP_CREATED);
    }

    /**
     * @param ShowJobRequest $request
     * @param int $id
     * @return JobResource
     */
    public function show(ShowJobRequest $request, int $id): JobResource
    {
        return JobResource::make(JobModel::find($id));
    }
}
