<?php

namespace App\Providers\Http;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\Http\ClientInterface as HttpClientInterface;
use App\Services\Http\Client as HttpClientService;

class ClientServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(HttpClientInterface::class, function () {
            return new HttpClientService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
