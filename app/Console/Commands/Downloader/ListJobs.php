<?php

namespace App\Console\Commands\Downloader;

use App\Models\Job as JobModel;
use Illuminate\Console\Command;
use App\Services\Resource as ResourceService;
use Psr\Container\ContainerInterface;

class ListJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'downloader:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of all downloader jobs.';

    /**
     * @var ResourceService
     */
    private $resourceService;

    /**
     * Create a new command instance.
     *
     * @param ContainerInterface $container
     * @return void
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->resourceService = $container->get('services.resource');
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $jobs = JobModel::with('file')->select([
            'id',
            'url',
            'status',
            'started_at',
            'finished_at',
            'error',
        ])->get();

        if ($jobs->isEmpty()) {
            $this->info('Job queue is empty.');

            return;
        }

        $items = [];
        /**
         * @var JobModel $job
         */
        foreach ($jobs as $job) {
            $items[] = [
                $job->id,
                $job->url,
                $job->human_status,
                $job->started_at,
                $job->finished_at,
                $job->error,
                $job->file ? $this->resourceService->getDownloadLink($job->file) : null,
            ];
        }

        $this->table([
            'ID',
            'Url',
            'Status',
            'Started At',
            'Finished At',
            'Error',
            'Download Link',
        ], $items);
    }
}
