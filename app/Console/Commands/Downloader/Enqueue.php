<?php

namespace App\Console\Commands\Downloader;

use Illuminate\Console\Command;
use Illuminate\Validation\ValidationException;
use Exception;
use App\Services\Resource as ResourceService;
use Psr\Container\ContainerInterface;

class Enqueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'downloader:enqueue {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enqueue new job to download resource.';

    /**
     * @var ResourceService
     */
    private $resourceService;

    /**
     * Create a new command instance.
     *
     * @param ContainerInterface $container
     * @return void
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->resourceService = $container->get('services.resource');
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $url = $this->argument('url');

        try {
            $this->resourceService->createDownloadJob($url);
            $this->info('Job to download resource has been enqueued.');
        } catch (ValidationException $exception) {
            foreach ($exception->validator->errors()->all() as $error) {
                $this->error($error);
            }
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
