<?php
declare(strict_types=1);

namespace App\Exceptions\Downloader;

use App\Exceptions\AbstractException;

class UnableToStoreException extends AbstractException
{

}
