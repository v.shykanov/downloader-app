<?php
declare(strict_types=1);

namespace App\Exceptions\Http;

use Psr\Http\Client\RequestExceptionInterface;
use Psr\Http\Message\RequestInterface;

class RequestException extends \GuzzleHttp\Exception\RequestException implements RequestExceptionInterface
{
    /**
     * @return \Psr\Http\Message\RequestInterface
     */
    public function getRequest(): RequestInterface
    {
        return parent::getRequest();
    }
}
