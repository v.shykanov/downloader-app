<?php
declare(strict_types=1);

namespace App\Exceptions\Resource;

use App\Exceptions\AbstractException;

class DownloadException extends AbstractException
{

}
