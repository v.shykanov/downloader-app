<?php
declare(strict_types=1);

namespace App\Services\Http;

use GuzzleHttp\Client as GuzzleClient;
use App\Interfaces\Http\ClientInterface as HttpClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use App\Exceptions\Http\RequestException as HttpRequestException;
use App\Exceptions\Http\NetworkException as HttpNetworkException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class Client extends GuzzleClient implements HttpClientInterface
{
    /**
     * @param \Psr\Http\Message\RequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     * @throws HttpRequestException|HttpNetworkException|\Exception
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        try {
            return $this->send($request);
        } catch (RequestException $exception) {
            throw new HttpRequestException($exception->getMessage(), $request, $exception->getResponse(), $exception);
        } catch (GuzzleException $exception) {
            throw new HttpNetworkException($exception->getMessage(), $request,null, $exception);
        }
    }
}
