<?php
declare(strict_types=1);

namespace App\Services;

use App\Exceptions\Resource\DownloadException;
use App\Interfaces\Http\ClientInterface as HttpClientInterface;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\RequestInterface;

class Downloader
{
    /**
     * @var \App\Services\Http\Client
     */
    protected $httpClient;

    /**
     * @var UriInterface
     */
    protected $uri;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @param HttpClientInterface $httpClient
     * @return void
     */
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param \Psr\Http\Message\UriInterface $uri
     * @return void
     */
    public function setUri(UriInterface $uri): void
    {
        $this->uri = $uri;
    }

    /**
     * @param \Psr\Http\Message\RequestInterface $request
     * @return void
     */
    public function setRequest(RequestInterface $request): void
    {
        $this->request = $request;
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \App\Exceptions\Resource\DownloadException
     */
    public function getResource(): ResponseInterface
    {
        try {
            return $this->httpClient->sendRequest(
                $this->request ?? new Request('GET', $this->uri)
            );
        } catch (\Exception $exception) {
            throw new DownloadException('Unable to get resource.', 0, $exception);
        }
    }
}
