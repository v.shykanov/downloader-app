<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\File as FileModel;
use App\Models\Job as JobModel;
use App\Jobs\Downloader as DownloaderJob;
use Validator;

/**
 *
 */
class Resource
{
    /**
     * @param string $url
     * @param bool $enqueue
     * @return JobModel
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createDownloadJob(string $url, bool $enqueue = true): JobModel
    {
        Validator::make([
            'url' => $url,
        ], [
            'url' => ['required', 'min:10', 'url', 'active_url'],
        ])->validate();

        $job = JobModel::create([
            'url' => $url,
        ]);

        if ($enqueue) {
            DownloaderJob::dispatch($job);
        }

        return $job;
    }

    /**
     * @param FileModel $file
     * @return string
     */
    public function getDownloadLink(FileModel $file): string
    {
        return route('api.files.download', $file->id);
    }
}
