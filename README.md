Requirements
------------
 - PHP >= 7.1.3
 - Laravel >= 5.5.0

Installation
------------

Run containers:
```
docker-compose up -d
```

Enter the workspace container:
```
docker-compose exec workspace bash
```

Create your local ```.env``` config:  
```
cp .env.example .env
```
```
php atisan key:generate
```

Install dependencies:
```
composer install
```
```
yarn
```

Run migrations:
```
php artisan migrate
```
```
php artisan migrate --env=testing
```

Usage
----------
Access the application in your browser at ```http://localhost:8111```.


Tests
-----

Run ```composer test``` inside ```workspace``` container


API
---
|Method |  URI		   					| Description			|
|---	|-----							|-------				|
|  GET	| /api/v1/jobs 					|   Get all jobs		|
|  GET	| /api/v1/jobs/{id}				|	Get one job			|
|  POST	| /api/v1/jobs					|	Push one job		|
|  GET	| /api/v1/files 				|   Get all files		|
|  GET	| /api/v1/files/{id}			|   Get one file		|
|  GET	| /api/v1/files/{id}/download	|   Download one file	|


Console Commands
----------------

| Command 		  			| Description		|
|------	  		  			|------				|
| downloader:list 			|  	List all jobs	|
| downloader:enqueue {url}  |  	Enqueue job		|
