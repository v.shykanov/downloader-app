<?php

namespace Tests\Unit\Services;

use App\Services\Downloader as DownloaderService;
use Tests\TestCase;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7;
use App\Services\Http\Client as HttpClientService;
use App\Exceptions\Resource\DownloadException;

class DownloaderTest extends TestCase
{
    /**
     * @expectedException \App\Exceptions\Resource\DownloadException
     *
     * @return void
     * @throws DownloadException
     */
    public function testThrowsRequestExceptionOnNotFound(): void
    {
        $mock = new MockHandler([
            new Response(404),
        ]);

        $handler = HandlerStack::create($mock);
        $downloaderService = new DownloaderService(new HttpClientService(['handler' => $handler]));

        $url = 'http://valid.com/foo.jpg';
        $downloaderService->setUri(new Psr7\Uri($url));
        $downloaderService->getResource();
    }

    /**
     * @return void
     * @throws DownloadException
     */
    public function testReturnsCorrectResponseOnOk(): void
    {
        $mock = new MockHandler([
            new Response(200, [
                'Content-Type' => 'text/plain',
                'Content-Length' => 3,
            ], 'foo'),
        ]);

        $handler = HandlerStack::create($mock);
        $downloaderService = new DownloaderService(new HttpClientService(['handler' => $handler]));

        $url = 'http://valid.com/foo.jpg';
        $downloaderService->setUri(new Psr7\Uri($url));
        $response = $downloaderService->getResource();

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('text/plain', $response->getHeaderLine('Content-Type'));
        $this->assertSame('3', $response->getHeaderLine('Content-Length'));
        $this->assertSame('foo', $response->getBody()->getContents());
    }
}
