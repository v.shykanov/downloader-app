<?php

namespace Tests\Unit\Services;

use App\Models\File as FileModel;
use App\Services\Resource as ResourceService;
use Tests\TestCase;
use Illuminate\Validation\ValidationException;

class ResourceTest extends TestCase
{
    /** @var ResourceService */
    protected $resourceService;

    public function setUp(): void
    {
        parent::setUp();
        $this->resourceService = resolve('services.resource');
    }

    /**
     * @return array
     */
    public function invalidUrlProvider(): array
    {
        return [
            [false],
            ['foo'],
            ['http://foo bar'],
            ['http://foo.bar'],
            ['http://not_valid/some'],
        ];
    }

    /**
     * @param string $url
     * @dataProvider invalidUrlProvider
     * @throws ValidationException
     */
    public function testThrowsValidationExceptionOnInvalidUrl(string $url): void
    {
        $this->expectException(ValidationException::class);
        $this->resourceService->createDownloadJob($url);
    }

    /**
     * @return void
     */
    public function testReturnsDownloadLink(): void
    {
        $fileModel = factory(FileModel::class)->make([
            'id' => 1,
        ]);

        $this->assertSame(
            route('api.files.download', 1),
            $this->resourceService->getDownloadLink($fileModel)
        );
    }
}
