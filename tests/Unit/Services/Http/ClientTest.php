<?php

namespace Tests\Unit\Services\Http;

use Tests\TestCase;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7;
use App\Services\Http\Client as HttpClientService;

class ClientTest extends TestCase
{
    /**
     * @expectedException \App\Exceptions\Http\RequestException
     *
     * @return void
     * @throws \Exception
     */
    public function testThrowsRequestExceptionOnNotFound(): void
    {
        $mock = new MockHandler([
            new Response(404),
        ]);

        $handler = HandlerStack::create($mock);
        $httpClientService = new HttpClientService(['handler' => $handler]);

        $request = new Psr7\Request('GET', 'http://valid.com/foo.jpg');
        $httpClientService->sendRequest($request);
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testReturnsCorrectResponseOnOk(): void
    {
        $mock = new MockHandler([
            new Response(200, [
                'Content-Type' => 'text/plain',
                'Content-Length' => 3,
            ], 'foo'),
        ]);

        $handler = HandlerStack::create($mock);
        $httpClientService = new HttpClientService(['handler' => $handler]);

        $request = new Psr7\Request('GET', 'http://valid.com/foo.jpg');
        $response = $httpClientService->sendRequest($request);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('text/plain', $response->getHeaderLine('Content-Type'));
        $this->assertSame('3', $response->getHeaderLine('Content-Length'));
        $this->assertSame('foo', $response->getBody()->getContents());
    }
}
