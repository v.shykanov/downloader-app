<?php

namespace Tests\Feature\Services;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Services\Resource as ResourceService;
use App\Jobs\Downloader as DownloaderJob;
use Queue;

class ResourceTest extends TestCase
{
    use RefreshDatabase;

    /** @var ResourceService */
    protected $resourceService;

    public function setUp(): void
    {
        parent::setUp();
        $this->resourceService = resolve('services.resource');
    }

    /**
     * @return void
     */
    public function testReturnsJobModel(): void
    {
        $url = 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/ESO-VLT-Laser-phot-33a-07.jpg/320px-ESO-VLT-Laser-phot-33a-07.jpg';
        $jobModel = $this->resourceService->createDownloadJob($url, false);
        $this->assertSame($url, $jobModel->url);
    }

    /**
     * @return void
     */
    public function testPushedDownloaderJob(): void
    {
        Queue::fake();

        $url = 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/ESO-VLT-Laser-phot-33a-07.jpg/320px-ESO-VLT-Laser-phot-33a-07.jpg';
        $this->resourceService->createDownloadJob($url);
        Queue::assertPushed(DownloaderJob::class, 1);
    }

}
