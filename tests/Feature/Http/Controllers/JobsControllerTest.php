<?php

namespace Tests\Feature\Http\Controllers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class JobsControllerTest extends TestCase
{
    /**
     * @return void
     */
    public function testJobs(): void
    {
        $response = $this->get('/jobs');

        $response->assertStatus(200);
    }
}
