<?php

namespace Tests\Feature\Http\Controllers\Api;

use App\Models\Job;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Job as JobModel;
use App\Models\File as FileModel;
use App\Http\Resources\Job as JobResource;
use Queue;
use Illuminate\Http\JsonResponse;

class JobControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        Queue::fake();
    }

    /**
     * @return void
     */
    public function test_can_create_job(): void
    {
        $response = $this->postJson(route('api.jobs.store'), [
            'url' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/PIA17202_-_Approaching_Enceladus.jpg/240px-PIA17202_-_Approaching_Enceladus.jpg',
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonFragment([
                'status' => true,
            ]);
    }

    /**
     * @return void
     */
    public function test_can_show_job(): void
    {
        $job = factory(JobModel::class)
            ->state('new')
            ->create();

        $job
            ->each(function (JobModel $job) {
                $job->file()->save(factory(FileModel::class)->make());
            });

        $response = $this->getJson(route('api.jobs.show', $job->id));

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => JsonResponse::create(JobResource::make($job))->getData(true)
            ])
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'url',
                    'status',
                    'file',
                ],
            ]);
    }

    /**
     * @return void
     */
    public function test_can_list_jobs(): void
    {
        $jobs = factory(JobModel::class, 2)
            ->state('finished')
            ->create()
            ->each(function (JobModel $job) {
                $job->file()->save(factory(FileModel::class)->make());
            })
            ->map(function ($job) {
                return $job;
            });

        $response = $this->getJson(route('api.jobs.index'));

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => JsonResponse::create(JobResource::collection($jobs))->getData(true)
            ])
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'url',
                        'status',
                        'file',
                    ],
                ],
            ]);

    }
}
