<?php

namespace Tests\Feature\Http\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\File as FileModel;
use App\Http\Resources\File as FileResource;
use Illuminate\Http\File;
use Illuminate\Http\JsonResponse;
use Storage;

class FileControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testCanShowFile(): void
    {
        $file = factory(FileModel::class)->create();

        $response = $this->getJson(route('api.files.show', $file->id));

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => JsonResponse::create(FileResource::make($file))->getData(true)
            ])
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'filename',
                    'size',
                    'mime',
                ],
            ]);
    }

    /**
     * @return void
     */
    public function testCanListFiles(): void
    {
        $files = factory(FileModel::class, 2)
            ->create()
            ->map(function ($file) {
                return $file;
            });

        $response = $this->getJson(route('api.files.index'));

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => JsonResponse::create(FileResource::collection($files))->getData(true)
            ])
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'filename',
                        'size',
                        'mime',
                    ],
                ],
            ]);
    }

    /**
     * @return void
     */
    public function testCanDownloadFile(): void
    {
        Storage::fake();

        $file = 'tests/data/milky-way.jpg';
        $mimeType = 'image/jpeg';
        $originalFilename = basename($file);
        $storedFilename = 'foo.jpg';

        $file = new File(base_path('tests/data/milky-way.jpg'));

        $fileModel = factory(FileModel::class)->create([
            'name' => $originalFilename,
            'filename' => $storedFilename,
            'mime' => $mimeType,
        ]);

        Storage::putFileAs('/', $file, $storedFilename);

        $response = $this->getJson(route('api.files.download', $fileModel->id));

        $response
            ->assertStatus(200)
            ->assertHeader('Content-Type', $mimeType);

        $this->assertSame(
            $response->headers->get('content-disposition'),
            "attachment; filename={$originalFilename}"
        );
    }
}
